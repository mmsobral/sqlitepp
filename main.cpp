#include <iostream>
#include <sqlite3.h>
#include "Sqlite3.h"

using namespace std;

#define DBpath "../data/dados.db"


int main() {
    Sqlite3 db(DBpath);

    try {
        auto res = db.exec("select * from Usuario");
        auto result = res.fetchall();
        for (auto & row: *result) {
        //for (auto & row: res) {
            for (auto &par: row) {
                ColumnData &data = par.second;
                string tipo;
                if (data.is_int()) tipo = "int";
                else if (data.is_float()) tipo = "float";
                else if (data.is_text()) tipo = "text";

                cout << par.first << " = " << par.second << "(" << tipo << "), ";

            }
            cout << endl;

        }
    } catch (SqliteException & e) {
        cout << "Erro: " << e.what() << endl;
    }
    return 0;
}