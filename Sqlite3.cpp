//
// Created by msobral on 19/08/2019.
//

#include "Sqlite3.h"

ostream& operator<<(ostream & out, const ColumnData & data) {
    out << data.str();
    return out;
}

Sqlite3::Sqlite3(const string &path) {
    if (sqlite3_open(path.c_str(), &pdb) != SQLITE_OK) {
        throw SqliteException(pdb);
        //sqlite3_close(pdb);
    }
}

Sqlite3::~Sqlite3() {
    sqlite3_close(pdb);
}

Result Sqlite3::exec(const string &sql) {
    sqlite3_stmt * cmd;

    if (sqlite3_prepare_v2(pdb, sql.c_str(), -1, &cmd, NULL) != SQLITE_OK) {
        throw SqliteException(pdb);
    }
    Result res(cmd, pdb);
    return res;
}

Result::Result(sqlite3_stmt * a_stmt, sqlite3 * a_pdb): p_stmt(a_stmt), pdb(a_pdb) {
}

Result::~Result() {
    sqlite3_finalize(p_stmt);
}

Result::iterator Result::begin() {
    return Result::iterator(p_stmt, pdb);
}

shared_ptr<vector<Row>> Result::fetchall() {
    shared_ptr<vector<Row>> ptr(new vector<Row>);

    for (Result::iterator it=begin(); it != end(); ++it) {
        ptr->push_back(*it);
    }

    return ptr;
}

Result::iterator Result::end() {
    return Result::iterator(nullptr,pdb);
}

Result::iterator::iterator(sqlite3_stmt *a_stmt, sqlite3 * a_pdb): stmt(a_stmt), pdb(a_pdb) {
    if (stmt != nullptr) ++(*this);
}

Result::iterator::iterator(const Result::iterator &it): stmt(it.stmt), pdb(it.pdb), row(it.row) {}

bool Result::iterator::operator==(const Result::iterator &it) const {
    return stmt == it.stmt;// and finished == it.finished;
}

bool Result::iterator::operator!=(const Result::iterator &it) const {
    return stmt != it.stmt;// or finished != it.finished;
}

Row& Result::iterator::operator*() {
    if (stmt == nullptr) throw SqliteException();
    return row;
}

Row* Result::iterator::operator->() {
    if (stmt == nullptr) throw SqliteException();
    return &row;
}

Result::iterator& Result::iterator::operator++() {
    //if (stmt == nullptr) throw -1;

    if (stmt == nullptr) {
        throw SqliteException();
    }

    row.clear();

    switch (sqlite3_step(stmt)) {
        case SQLITE_DONE:
            stmt = nullptr;
            break;
        case SQLITE_ROW: {
            int cols = sqlite3_column_count(stmt);
            for (int j=0; j < cols; j++) {
                auto name = sqlite3_column_name(stmt, j);
                ColumnData data(sqlite3_column_value(stmt, j));
                row[name] = data;
            }
            break;
        };
        default:
            throw SqliteException(pdb);
    }
    return *this;
}

Result::iterator& Result::iterator::operator++(int k) {
    auto ptr = this;
    ++(*this);
    return *ptr;
}
