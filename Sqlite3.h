//
// Created by msobral on 19/08/2019.
//

#ifndef DBTEST_SQLITE3_H
#define DBTEST_SQLITE3_H

#include <string>
#include <sqlite3.h>
#include <map>
#include <memory>
#include <vector>
#include <iterator>
#include <ostream>
#include "SqliteException.h"

using std::string;
using std::map;
using std::shared_ptr;
using std::unique_ptr;
using std::vector;
using std::ostream;

struct ColumnData {
    int type;
    union Value {
        long i_val;
        string * s_val;
        double f_val;
    } val;

    ColumnData(): type(SQLITE_NULL) {
    }

    ~ColumnData() {
        switch (type) {
                case SQLITE_TEXT:
                case SQLITE_BLOB:
                    delete val.s_val;
                    break;
        }
    }

    ColumnData(sqlite3_value * value) {
        type = sqlite3_value_type(value);
        switch (type) {
            case SQLITE_INTEGER:
                val.i_val = sqlite3_value_int64(value);
                break;
            case SQLITE_FLOAT:
                val.f_val = sqlite3_value_double(value);
                break;
            case SQLITE_BLOB:
            case SQLITE_TEXT:
                //val.s_val.reset(new string((char*)sqlite3_value_text(value), sqlite3_value_bytes(value)));
                val.s_val = new string((char*)sqlite3_value_text(value), sqlite3_value_bytes(value));
                break;
            case SQLITE_NULL:
                break;
            default:
                throw SqliteException();
        }
    }

    ColumnData(const ColumnData & o) {
        *this = o;
    }

    ColumnData& operator=(const ColumnData& o) {
        type = o.type;
        switch (type) {
            case SQLITE_INTEGER:
                val.i_val = o.val.i_val;
                break;
            case SQLITE_FLOAT:
                val.f_val = o.val.f_val;
                break;
            case SQLITE_BLOB:
            case SQLITE_TEXT:
                val.s_val = new string(*o.val.s_val);
                break;
            case SQLITE_NULL:
                break;
            default:
                throw SqliteException();
        }
        return *this;
    }

    long get_int() const {
        if (type == SQLITE_INTEGER) return val.i_val;
        throw SqliteException();
    }

    double get_double() const {
        if (type == SQLITE_FLOAT) return val.f_val;
        throw SqliteException();
    }

    string get_text() const {
        if (type == SQLITE_TEXT) return *val.s_val;
        throw SqliteException();
    }

    string get_blob() const {
        if (type == SQLITE_TEXT) return *val.s_val;
        throw SqliteException();
    }

    string str() const {
        switch (type) {
            case SQLITE_INTEGER:
                return std::to_string(val.i_val);
                break;
            case SQLITE_FLOAT:
                return std::to_string(val.f_val);
                break;
            case SQLITE_BLOB:
            case SQLITE_TEXT:
                return *val.s_val;
                break;
            case SQLITE_NULL:
                return string();
                break;
            default:
                throw SqliteException();
        }
    }

    bool is_null() const { return type == SQLITE_NULL;}
    bool is_int() const { return type == SQLITE_INTEGER;}
    bool is_text() const { return type == SQLITE_TEXT;}
    bool is_float() const { return type == SQLITE_FLOAT;}
    bool is_blob() const { return type == SQLITE_BLOB;}
};

ostream& operator<<(ostream & out, const ColumnData & data);

typedef map<string,ColumnData> Row;

class Sqlite3;

class Result {
public:
    ~Result();

    class iterator: public std::iterator<std::forward_iterator_tag, Row> {
    private:
        friend Result;
        sqlite3_stmt * stmt;
        sqlite3 * pdb;
        Row row;

        iterator(sqlite3_stmt * a_stmt, sqlite3 * a_pdb);
    public:
        iterator(const iterator & it);

        iterator& operator++();
        iterator& operator++(int k);
        bool operator==(const iterator& it) const;
        bool operator!=(const iterator& it) const;
        Row& operator*();
        Row* operator->();
    };

    iterator begin();
    iterator end();

    // retorna todo o resultado
    shared_ptr<vector<Row>> fetchall();

private:
    sqlite3_stmt * p_stmt;
    sqlite3 * pdb;

    friend Sqlite3;

    Result(sqlite3_stmt * a_stmt, sqlite3 * a_pdb);
};

class Sqlite3 {
public:
    Sqlite3(const string & path);
    virtual ~Sqlite3();

    Result exec(const string & sql);

protected:
    sqlite3 * pdb;
};


#endif //DBTEST_SQLITE3_H
