//
// Created by sobral on 24/08/2019.
//

#include "SqliteException.h"

SqliteException::SqliteException() noexcept: exception(),exc_code(SQLITE_ERROR) {
}

SqliteException::SqliteException(sqlite3 *pdb) noexcept: exception() {
    exc_code = sqlite3_errcode(pdb);
    msg = sqlite3_errmsg(pdb);
}

SqliteException::SqliteException(const SqliteException & o) noexcept: exception(),exc_code(o.exc_code),msg(o.msg) {

}

SqliteException &SqliteException::operator=(const SqliteException & e) noexcept {
    exc_code = e.exc_code;
    msg = e.msg;
    return *this;
}

SqliteException::~SqliteException() {

}

const char *SqliteException::what() const noexcept {
    return msg.c_str();
}
