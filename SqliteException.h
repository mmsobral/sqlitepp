//
// Created by sobral on 24/08/2019.
//

#ifndef DBTEST_SQLITEEXCEPTION_H
#define DBTEST_SQLITEEXCEPTION_H

#include <exception>
#include <string>
#include <sqlite3.h>

using std::exception;
using std::string;

class SqliteException: public exception {
public:
    SqliteException () noexcept;
    SqliteException (sqlite3 * pdb) noexcept;
    SqliteException (const SqliteException&) noexcept;
    SqliteException& operator= (const SqliteException&) noexcept;
    virtual ~SqliteException();
    virtual const char* what() const noexcept;
protected:
    int exc_code;
    string msg;
};

#endif //DBTEST_SQLITEEXCEPTION_H
